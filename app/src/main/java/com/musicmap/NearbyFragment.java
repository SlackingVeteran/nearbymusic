package com.musicmap;

import android.Manifest;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.musicmap.utils.Constants;
import com.musicmap.utils.GeofireAdapter;

import static com.musicmap.GoogleMapFragment.MY_PERMISSIONS_REQUEST_LOCATION;

public class NearbyFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, FirebaseAuth.AuthStateListener {

    private static final String TAG = NearbyFragment.class.getSimpleName();

    protected FirebaseAuth mFirebaseAuth;
    private FirebaseUser mUser;
    private DatabaseReference mUserRef;
    private DatabaseReference mUserLocationRef;

    private ListView mListView;

    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 1;

    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private GeoLocation mCurrentGeoLocation;
    private GeoFire mGeoFire;
    private GeoQuery mGeoQuery;
    boolean mActiveGeoQuery = false;

    private GeofireAdapter mListAdapter;

    LocationListener mlocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            mLastLocation = location;
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("NearbyFragment", "Inside onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_nearby, container, false);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseAuth.addAuthStateListener(this);

        mUser = mFirebaseAuth.getCurrentUser();
        mUserLocationRef = FirebaseDatabase.getInstance()
                .getReference(Constants.FIREBASE_LOCATION_USERS);
        mUserRef = FirebaseDatabase.getInstance()
                .getReference(Constants.FIREBASE_USERS);
        mGeoFire = new GeoFire(mUserLocationRef);

        mListView = (ListView) rootView.findViewById(R.id.nearby_results);
        mListAdapter = new GeofireAdapter(mUserLocationRef, getActivity(), R.layout.nearby_holder);

        setHasOptionsMenu(false);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            checkLocationPermission();


        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        return rootView;
    }

    private void initializeScreen() {

        Log.d("NearbyFragment", "mUserRef is not null");
        mGeoQuery = mGeoFire.queryAtLocation(mCurrentGeoLocation, 20);
        mGeoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                Log.d(TAG, "Key " + key + " entered the search area at [" + location.latitude + "," + location.longitude + "]");
                DatabaseReference tempRef = mUserRef.child(key);
                tempRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {

                        // I  add the deal only if it doesn't exist already in the adapter
                        String key = snapshot.getKey();
                        if (!mListAdapter.exists(key)) {
                            Log.d(TAG, "item added " + key);
                            mListAdapter.addSingle(snapshot);
                            mListAdapter.notifyDataSetChanged();
                        } else {
                            //...otherwise I will update the record
                            Log.d(TAG, "item updated: " + key);
                            mListAdapter.update(snapshot, key);
                            mListAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d(TAG, "cancelled with error:" + databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onKeyExited(String key) {
                Log.d(TAG, "deal " + key + " is no longer in the search area");
                mListAdapter.remove(key);
            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {
                Log.d(TAG, String.format("Key " + key + " moved within the search area to [%f,%f]", location.latitude, location.longitude));
            }

            @Override
            public void onGeoQueryReady() {
                Log.d(TAG, "All initial data has been loaded and events have been fired!");
                mActiveGeoQuery = true;
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                Log.e(TAG, "There was an error with this query: " + error);
                mActiveGeoQuery = false;
            }
        });
        mListView.setAdapter(mListAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mCurrentGeoLocation != null && mCurrentGeoLocation !=null && !mActiveGeoQuery) {
            initializeScreen();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "Location services connected.");
        //Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10*1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,mlocationListener);
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mCurrentGeoLocation = new GeoLocation(mLastLocation.getLatitude(),mLastLocation.getLongitude());
            Log.d("log","lat lon "+ mLastLocation.getLatitude()+" "+ mLastLocation.getLongitude());
            if (!mActiveGeoQuery) {
                initializeScreen();
                mListAdapter.notifyDataSetChanged();
            }
        }
        else if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("log", "do nothing ");
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Location services suspended. Please reconnect.");
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("log", "Inside onConnection Failed");
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                Log.e("log", "Exception error message : " + e.getMessage());
            }
        } else
            Log.d("log", "Location services connection failed with code " + connectionResult.getErrorCode());
    }

    @Override
    public void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }
    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    // Check Runtime permissions for Marshmallow and above (API 23+) devices
    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        // Do nothing
    }
}
