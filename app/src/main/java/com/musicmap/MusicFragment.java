package com.musicmap;

import android.Manifest;
import android.animation.LayoutTransition;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.musicmap.utils.Constants;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Error;
import com.spotify.sdk.android.player.Metadata;
import com.spotify.sdk.android.player.PlaybackState;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerEvent;
import com.spotify.sdk.android.player.SpotifyPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kaaes.spotify.webapi.android.models.Track;

import static com.musicmap.GoogleMapFragment.MY_PERMISSIONS_REQUEST_LOCATION;

public class MusicFragment extends Fragment implements Search.View, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    public List<Track> listTrack = new ArrayList<>();

    private String token;
    private Search.ActionListener mActionListener;
    private LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
    private ScrollListener mScrollListener = new ScrollListener(mLayoutManager);
    private SearchResultsAdapter mAdapter;

    private static final String TAG = MusicFragment.class.getSimpleName();

    private SpotifyPlayer mPlayer;
    private PlaybackState mCurrentPlaybackState;
    private Metadata mMetadata;

    private String mNextTrackUri;
    private int mCurrentIndex;

    private ImageButton mPlayPause;
    private ImageButton mPlayPauseCenter;
    private ImageButton mSkipNext;
    private ImageButton mSkipPrevious;
    private TextView mTitle;
    private TextView mSubtitle;
    private ImageView mAlbumArt;
    private String mArtUrl;

    protected FirebaseAuth.AuthStateListener mAuthListener;
    protected FirebaseAuth mFirebaseAuth;
    private FirebaseUser mUser;
    private DatabaseReference mUserRef;
    private DatabaseReference mUserLocationRef;

    private Map mEmptyTrackDetail;
    private Map mTrackDetail;

    private static final String KEY_CURRENT_QUERY = "CURRENT_QUERY";
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 1;

    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private GeoLocation mCurrentGeoLocation;
    private GeoFire mGeoFire;

    LocationListener mlocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            mLastLocation = location;
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_music, container, false);
        setHasOptionsMenu(true);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            checkLocationPermission();

        MainActivity activity = (MainActivity) getActivity();
        token = activity.getAccessToken();

        mPlayPause = (ImageButton) rootView.findViewById(R.id.play_pause);
        mPlayPauseCenter = (ImageButton) rootView.findViewById(R.id.play_pause_center);
        mSkipNext = (ImageButton) rootView.findViewById(R.id.skip_next);
        mSkipPrevious = (ImageButton) rootView.findViewById(R.id.skip_previous);

        mPlayPause.setEnabled(true);
        mPlayPauseCenter.setEnabled(true);
        mSkipNext.setEnabled(true);
        mSkipPrevious.setEnabled(true);
        mPlayPause.setOnClickListener(mPlayPauseListener);
        mPlayPauseCenter.setOnClickListener(mPlayPauseListener);
        mSkipNext.setOnClickListener(mNextListener);
        mSkipPrevious.setOnClickListener(mPrevListener);

        mPlayer = ((App) getActivity().getApplication()).getSpotifyPlayer();
        mPlayer.addNotificationCallback(mNotificationCallback);
        mPlayer.addConnectionStateCallback(mConnectionStateCallback);
        mTitle = (TextView) rootView.findViewById(R.id.title);
        mSubtitle = (TextView) rootView.findViewById(R.id.artist);
        mAlbumArt = (ImageView) rootView.findViewById(R.id.album_art);

        mActionListener = new SearchPresenter(this.getContext(), this);
        mActionListener.init(token);

        mFirebaseAuth = FirebaseAuth.getInstance();

        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                mUser = firebaseAuth.getCurrentUser();
                    /* The user has been logged out */
                if (mUser != null) {
                    Log.d("MusicFragment", "User ID: "+mUser.getUid());
                    mUserLocationRef = FirebaseDatabase.getInstance()
                            .getReference(Constants.FIREBASE_LOCATION_USERS);
                    mUserRef = FirebaseDatabase.getInstance()
                            .getReference(Constants.FIREBASE_USERS)
                            .child(mUser.getUid());
                    mEmptyTrackDetail = new HashMap();
                    mTrackDetail = new HashMap();
                    mGeoFire = new GeoFire(mUserLocationRef);
                }
            }
        };
        mFirebaseAuth.addAuthStateListener(mAuthListener);
        if (mUserRef == null) {
            Log.d("MusicFragment", "User Ref is null ");
        }

        // Setup search results list
        mAdapter = new SearchResultsAdapter(getContext(), new SearchResultsAdapter.ItemSelectedListener() {
            @Override
            public void onItemSelected(View itemView, Track item) {
                Log.d("MusicFragment","Item is selected");
                mPlayer.playUri(mOperationCallback,item.uri,0,0);
                mCurrentIndex = listTrack.indexOf(item);

            }
        });

        RecyclerView resultsList = (RecyclerView) rootView.findViewById(R.id.search_results);
        resultsList.setHasFixedSize(true);
        resultsList.setLayoutManager(mLayoutManager);
        resultsList.setAdapter(mAdapter);
        resultsList.addOnScrollListener(mScrollListener);

        LinearLayout controlView = (LinearLayout) rootView.findViewById(R.id.playback_controls);
        controlView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout multiControl = (LinearLayout) getActivity().findViewById(R.id.multi_control);

                if (multiControl.getVisibility() == View.GONE) {
                    multiControl.setVisibility(LinearLayout.VISIBLE);
                    mPlayPause.setVisibility(View.GONE);
                }
                else {
                    multiControl.setVisibility(LinearLayout.GONE);
                    mPlayPause.setVisibility(View.VISIBLE);
                }
            }
        });

        // If Activity was recreated wit active search restore it
        if (savedInstanceState != null) {
            String currentQuery = savedInstanceState.getString(KEY_CURRENT_QUERY);
            mActionListener.search(currentQuery);
        }

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        Log.d("MusicFragment","Creating menu");
        inflater.inflate(R.menu.music_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        final SearchView searchView = new SearchView(((MainActivity) getContext()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, searchView);

        LinearLayout searchBar = (LinearLayout) searchView.findViewById(R.id.search_bar);
        searchBar.setLayoutTransition(new LayoutTransition());
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mActionListener.search(query);
                searchView.clearFocus();
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private class ScrollListener extends ResultListScrollListener {

        public ScrollListener(LinearLayoutManager layoutManager) {
            super(layoutManager);
        }

        @Override
        public void onLoadMore() {
            mActionListener.loadMoreResults();
        }
    }

    @Override
    public void reset() {
        mScrollListener.reset();
        mAdapter.clearData();
    }

    @Override
    public void addData(List<Track> items) {
        mAdapter.addData(items);
        if (items != null) {
            for (int i = 0; i <= items.size(); i++) {
                listTrack.addAll(items);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActionListener.getCurrentQuery() != null) {
            outState.putString(KEY_CURRENT_QUERY, mActionListener.getCurrentQuery());
        }
    }

    private void onMetadataChanged() {
        logMessage("Metadata changed");
        if (getActivity() == null) {
            return;
        }
        if (mMetadata == null) {
            return;
        }
        int nextIndex = mCurrentIndex + 1;
        mNextTrackUri = listTrack.get(nextIndex).uri;
        mPlayer.queue(mOperationCallback, mNextTrackUri);
        mCurrentIndex++;

        mTitle.setText(mMetadata.currentTrack.name);
        mSubtitle.setText(mMetadata.currentTrack.albumName);
        mArtUrl = mMetadata.currentTrack.albumCoverWebUrl;

        if (mArtUrl != null) {
            Glide.with(getActivity()).load(mArtUrl).into(mAlbumArt);
        }
        mTrackDetail.put("trackTitle", mMetadata.currentTrack.name);
        mTrackDetail.put("trackAlbum", mMetadata.currentTrack.albumName);
        mTrackDetail.put("albumArtUrl", mMetadata.currentTrack.albumCoverWebUrl);
        mTrackDetail.put("trackUri", mMetadata.currentTrack.uri);

        mUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    mUserRef.updateChildren(mTrackDetail);
                }
            }

            @Override public void onCancelled(DatabaseError databaseError) {

            }
        });
        if (mCurrentGeoLocation != null) {
            mGeoFire.setLocation(mUser.getUid(), mCurrentGeoLocation);
        }
    }

    protected void showPlaybackControls() {
        Log.d("Music Fragment", "showPlaybackControls");
        LinearLayout playbackControls = (LinearLayout) getView().findViewById(R.id.playback_controls);
        if (playbackControls.getVisibility() == View.GONE) {
//            playbackControls.setAlpha(0.0f);
            playbackControls.setVisibility(RelativeLayout.VISIBLE);
//            // Start the animation
//            playbackControls.animate()
//                    .translationY(playbackControls.getHeight())
//                    .setDuration(1000)
//                    .alpha(1.0f);
        }
    }

    protected void hidePlaybackControls() {
        Log.d("Music Fragment", "hidePlaybackControls");
        LinearLayout playbackControls = (LinearLayout) getActivity().findViewById(R.id.playback_controls);
        playbackControls.setVisibility(LinearLayout.GONE);
    }

    private final View.OnClickListener mPlayPauseListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mPlayer == null) return;

            switch (v.getId()) {
                case R.id.play_pause:case R.id.play_pause_center:
                    if (mCurrentPlaybackState.isPlaying) {
                        mPlayer.pause(mOperationCallback);

                    }
                    else {
                        mPlayer.resume(mOperationCallback);
                    }
            }
        }
    };

    private final View.OnClickListener mNextListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mPlayer == null) return;

            switch (v.getId()) {
                case R.id.skip_next:
                    mPlayer.skipToNext(mOperationCallback);

            }
        }
    };
    private final View.OnClickListener mPrevListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mPlayer == null) return;

            switch (v.getId()) {
                case R.id.skip_previous:
                    mPlayer.skipToPrevious(mOperationCallback);
            }
        }
    };

    private final Player.NotificationCallback mNotificationCallback = new Player.NotificationCallback() {
        @Override
        public void onPlaybackEvent(PlayerEvent event) {
            // Remember kids, always use the English locale when changing case for non-UI strings!
            // Otherwise you'll end up with mysterious errors when running in the Turkish locale.
            // See: http://java.sys-con.com/node/46241
            mCurrentPlaybackState = mPlayer.getPlaybackState();
            mMetadata = mPlayer.getMetadata();
//            Log.i("Music Fragment", "Player state: " + mCurrentPlaybackState);
            Log.i("Music Fragment", "Metadata: " + mMetadata);
            logMessage("Event: " + event);
            if (mMetadata.currentTrack != null && event.toString().equals("kSpPlaybackEventAudioFlush")) {
                onMetadataChanged();
            }
            if (mCurrentPlaybackState.isPlaying) {
                mPlayPause.setImageDrawable(
                        ContextCompat.getDrawable(getActivity(), R.drawable.ic_pause_black_36dp));
                mPlayPauseCenter.setImageDrawable(
                        ContextCompat.getDrawable(getActivity(), R.drawable.ic_pause_black_36dp));
                setTrackPlaying(true);
            }
            else {
                mPlayPause.setImageDrawable(
                        ContextCompat.getDrawable(getActivity(), R.drawable.ic_play_arrow_black_36dp));
                mPlayPauseCenter.setImageDrawable(
                        ContextCompat.getDrawable(getActivity(), R.drawable.ic_play_arrow_black_36dp));
                setTrackPlaying(false);
            }
//
            if (mMetadata.currentTrack != null) {
                showPlaybackControls();
            }
            else {
                hidePlaybackControls();
            }
        }

        @Override
        public void onPlaybackError(Error error) {
            Log.d("Music Fragment","Err: " + error);
        }
    };

    private final ConnectionStateCallback mConnectionStateCallback = new ConnectionStateCallback() {
        @Override
        public void onLoggedIn() {
            logMessage("Login complete");

        }

        @Override
        public void onLoggedOut() {
            logMessage("Logout complete");

        }

        public void onLoginFailed(Error error) {
            logMessage("Login error "+ error);
        }

        @Override
        public void onTemporaryError() {
            logMessage("Temporary error occurred");
        }

        @Override
        public void onConnectionMessage(final String message) {
            logMessage("Incoming connection message: " + message);
        }
    };

    private final Player.OperationCallback mOperationCallback = new Player.OperationCallback() {
        @Override
        public void onSuccess() {
            Log.d("Music Fragment", "OK!");
        }

        @Override
        public void onError(Error error) {
            Log.d("Music Fragment", "ERROR:" + error);
        }
    };

    private void logMessage(String msg) {
        Log.d(TAG, msg);
    }

    public void setTrackPlaying(final boolean isTrackPlaying) {
        mUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    mUserRef.child(Constants.FIREBASE_PROPERTY_PlayingTrack)
                            .setValue(isTrackPlaying);
                    if (!isTrackPlaying) {
                        mEmptyTrackDetail.put("trackTitle", "");
                        mEmptyTrackDetail.put("trackAlbum", "");
                        mEmptyTrackDetail.put("albumArtUrl", "");
                        mEmptyTrackDetail.put("trackUri", "");
                        mUserRef.updateChildren(mEmptyTrackDetail);
                        if (mUser != null) {
                            mGeoFire.removeLocation(mUser.getUid());
                        }
                    }
                    else {
                        mUserRef.updateChildren(mTrackDetail);
                        if (mCurrentGeoLocation != null && mUser != null) {
                            mGeoFire.setLocation(mUser.getUid(), mCurrentGeoLocation);
                        }
                    }
                }
            }

            @Override public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "Location services connected.");
        //Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10*1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,mlocationListener);
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mCurrentGeoLocation = new GeoLocation(mLastLocation.getLatitude(),mLastLocation.getLongitude());
            Log.d("log","lat lon "+ mLastLocation.getLatitude()+" "+ mLastLocation.getLongitude());
        }
        else if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("log", "do nothing ");
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Location services suspended. Please reconnect.");
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("log", "Inside onConnection Failed");
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                Log.e("log", "Exception error message : " + e.getMessage());
            }
        } else
            Log.d("log", "Location services connection failed with code " + connectionResult.getErrorCode());
    }

    @Override
    public void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }
    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    // Check Runtime permissions for Marshmallow and above (API 23+) devices
    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.


                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }
}
