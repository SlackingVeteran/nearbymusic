package com.musicmap;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        NearbyFragment NearbyTab = new NearbyFragment();
        MusicFragment MusicTab = new MusicFragment();
        switch (position) {
            case 0:
                return MusicTab;
            case 1:
                return NearbyTab;
            default:
                return MusicTab;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}