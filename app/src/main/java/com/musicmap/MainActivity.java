package com.musicmap;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.musicmap.utils.Constants;

public class MainActivity extends AppCompatActivity {

    static final String EXTRA_TOKEN = "EXTRA_TOKEN";

    private String token;

    protected FirebaseAuth.AuthStateListener mAuthListener;
    protected FirebaseAuth mFirebaseAuth;
    private FirebaseUser mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        token = intent.getStringExtra(MainActivity.EXTRA_TOKEN);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Music"));
        tabLayout.addTab(tabLayout.newTab().setText("Nearby"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                mUser = firebaseAuth.getCurrentUser();
                    /* The user has been logged out */
                if (mUser == null) {
//                    takeUserToLoginScreenOnUnAuth();
                    Log.d("MainActivity", "Firebase user is not defined. " +
                            "In future will have to add a method to send it to login screen");

                }
                else {
                    setUserOnline(true);
                    Log.d("Main Activity", "User ID: "+mUser.getUid());
                }
            }
        };
        mFirebaseAuth.addAuthStateListener(mAuthListener);

        if (savedInstanceState == null) {
            final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
            final PagerAdapter adapter = new PagerAdapter
                    (getSupportFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(adapter);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        if (mUser != null) {
            setUserOnline(true);
        }
    }
    @Override protected void onPause() {
        super.onPause();
        if (mUser != null) {
            setUserOnline(false);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (mUser != null) {
            setUserOnline(true);
        }
    }
    private void setUserOnline(final boolean online) {
        final DatabaseReference userRef = FirebaseDatabase.getInstance()
                .getReference(Constants.FIREBASE_USERS)
                .child(mUser.getUid());

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    userRef.child(Constants.FIREBASE_PROPERTY_ONLINE)
                            .setValue(online);
                }
            }

            @Override public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public String getAccessToken() {
        return token;
    }
}