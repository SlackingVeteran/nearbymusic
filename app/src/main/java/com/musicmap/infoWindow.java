package com.musicmap;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class infoWindow extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_info_window, container, false);
        ImageView song_image = (ImageView) rootView.findViewById(R.id.song_image);
        TextView song_name = (TextView) rootView.findViewById(R.id.song_name);
        TextView song_artist = (TextView) rootView.findViewById(R.id.song_artist);
        song_name.setText("Song Name");
        song_artist.setText("Song Artist");
        Glide.with(getContext())
                .load("https://lh5.googleusercontent.com/-a0_lShjAU6c/AAAAAAAAAAI/AAAAAAAAAuI/rcJUy479ur8/s96-c/photo.jpg ").into(song_image);


        return rootView;
    }
}