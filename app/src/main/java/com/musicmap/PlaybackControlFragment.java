package com.musicmap;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.spotify.sdk.android.player.Error;
import com.spotify.sdk.android.player.Metadata;
import com.spotify.sdk.android.player.PlaybackState;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.SpotifyPlayer;


public class PlaybackControlFragment extends Fragment {

    private static final String TAG = MusicFragment.class.getSimpleName();

    private SpotifyPlayer mPlayer;
    private PlaybackState mCurrentPlaybackState;
    private Metadata mMetadata;

    private ImageButton mPlayPause;
    private ImageButton mPlayPauseCenter;
    private ImageButton mSkipNext;
    private ImageButton mSkipPrevious;
    private TextView mTitle;
    private TextView mSubtitle;
    private ImageView mAlbumArt;
    private String mArtUrl;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_music, container, false);
        setHasOptionsMenu(true);

        mPlayPause = (ImageButton) rootView.findViewById(R.id.play_pause);
        mPlayPauseCenter = (ImageButton) rootView.findViewById(R.id.play_pause_center);
        mSkipNext = (ImageButton) rootView.findViewById(R.id.skip_next);
        mSkipPrevious = (ImageButton) rootView.findViewById(R.id.skip_previous);

        mPlayPause.setEnabled(true);
        mPlayPauseCenter.setEnabled(true);
        mSkipNext.setEnabled(true);
        mSkipPrevious.setEnabled(true);
        mPlayPause.setOnClickListener(mPlayPauseListener);
        mPlayPauseCenter.setOnClickListener(mPlayPauseListener);
        mSkipNext.setOnClickListener(mNextListener);
        mSkipPrevious.setOnClickListener(mPrevListener);

        mPlayer = ((App) getActivity().getApplication()).getSpotifyPlayer();

        mTitle = (TextView) rootView.findViewById(R.id.title);
        mSubtitle = (TextView) rootView.findViewById(R.id.artist);
        mAlbumArt = (ImageView) rootView.findViewById(R.id.album_art);

//        LinearLayout controlView = (LinearLayout) rootView.findViewById(R.id.playback_controls);
//        controlView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                LinearLayout multiControl = (LinearLayout) getActivity().findViewById(R.id.multi_control);
//
//                if (multiControl.getVisibility() == View.GONE) {
//                    multiControl.setVisibility(LinearLayout.VISIBLE);
//                    mPlayPause.setVisibility(View.GONE);
//                }
//                else {
//                    multiControl.setVisibility(LinearLayout.GONE);
//                    mPlayPause.setVisibility(View.VISIBLE);
//                }
//            }
//        });

        // If Activity was recreated wit active search restore it
        if (savedInstanceState != null) {
            // Do Something
        }

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void onMetadataChanged() {
        logMessage("Metadata changed");
        if (getActivity() == null) {
            return;
        }
        if (mMetadata == null) {
            return;
        }

        mTitle.setText(mMetadata.currentTrack.name);
        mSubtitle.setText(mMetadata.currentTrack.albumName);
        mArtUrl = mMetadata.currentTrack.albumCoverWebUrl;

        if (mArtUrl != null) {
            Glide.with(getActivity()).load(mArtUrl).into(mAlbumArt);
        }
    }

    private final View.OnClickListener mPlayPauseListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mPlayer == null) return;

            switch (v.getId()) {
                case R.id.play_pause:case R.id.play_pause_center:
                    if (mCurrentPlaybackState.isPlaying) {
                        mPlayer.pause(mOperationCallback);

                    }
                    else {
                        mPlayer.resume(mOperationCallback);
                    }
            }
        }
    };

    private final View.OnClickListener mNextListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mPlayer == null) return;

            switch (v.getId()) {
                case R.id.skip_next:
                    mPlayer.skipToNext(mOperationCallback);

            }
        }
    };
    private final View.OnClickListener mPrevListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mPlayer == null) return;

            switch (v.getId()) {
                case R.id.skip_previous:
                    mPlayer.skipToPrevious(mOperationCallback);
            }
        }
    };

    public void updatePlaybackController() {
        // Remember kids, always use the English locale when changing case for non-UI strings!
        // Otherwise you'll end up with mysterious errors when running in the Turkish locale.
        // See: http://java.sys-con.com/node/46241
        mCurrentPlaybackState = mPlayer.getPlaybackState();
        mMetadata = mPlayer.getMetadata();

        onMetadataChanged();

        if (mCurrentPlaybackState.isPlaying) {
            mPlayPause.setImageDrawable(
                    ContextCompat.getDrawable(getActivity(), R.drawable.ic_pause_black_36dp));
            mPlayPauseCenter.setImageDrawable(
                    ContextCompat.getDrawable(getActivity(), R.drawable.ic_pause_black_36dp));
        }
        else {
            mPlayPause.setImageDrawable(
                    ContextCompat.getDrawable(getActivity(), R.drawable.ic_play_arrow_black_36dp));
            mPlayPauseCenter.setImageDrawable(
                    ContextCompat.getDrawable(getActivity(), R.drawable.ic_play_arrow_black_36dp));
        }
    }

    private final Player.OperationCallback mOperationCallback = new Player.OperationCallback() {
        @Override
        public void onSuccess() {
            Log.d("Music Fragment", "OK!");
        }

        @Override
        public void onError(Error error) {
            Log.d("Music Fragment", "ERROR:" + error);
        }
    };

    private void logMessage(String msg) {
        Log.d(TAG, msg);
    }

}
