package com.musicmap;

import android.app.Application;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.spotify.sdk.android.player.SpotifyPlayer;

public class App extends Application {

    private SpotifyPlayer spotifyPlayer;

    @Override
    public void onCreate() {
        super.onCreate();

        if (!FirebaseApp.getApps(this).isEmpty()) {
            Log.d("FIREBASE", "Enabling persinstence!");
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }
    }

    public SpotifyPlayer getSpotifyPlayer() {
        return spotifyPlayer;
    }

    public void setSpotifyPlayer(SpotifyPlayer spotifyPlayer) {
        this.spotifyPlayer = spotifyPlayer;
    }
}
