package com.musicmap.utils;

public final class Constants {


    /**
     * Constants para bundles, extras e keys shared preferences
     */
    public static final String KEY_PROVIDER = "PROVIDER";
    public static final String KEY_ENCODED_EMAIL = "ENCODED_EMAIL";
    public static final String GOOGLE_PROVIDER = "google";
    public static final String CLIENT_ID = "9173be58a3924504a7b08d8ad41ea68f";
    public static final String FIREBASE_USERS = "user";
    public static final String FIREBASE_LOCATION_USERS = "user_location";
    public static final String FIREBASE_PROPERTY_TIMESTAMP = "timestamp";
    public static final String FIREBASE_PROPERTY_USER_DEVICE_ID = "fcmUserDeviceId";
    public static final String KEY_USER_EMAIL = "USER_EMAIL";
    public static final String KEY_USER_DISPLAY_NAME = "USER_DISPLAY_NAME";
    public static final String KEY_USER_PROVIDER_PHOTO_URL = "USER_PROVIDER_PHOTO_URL";
    public static final String FIREBASE_PROPERTY_ONLINE = "online";
    public static final String FIREBASE_PROPERTY_PlayingTrack = "playingTrack";

}
