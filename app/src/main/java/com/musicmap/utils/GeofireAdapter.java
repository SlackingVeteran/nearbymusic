package com.musicmap.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.Query;
import com.musicmap.R;
import com.musicmap.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by suyas on 1/31/2017.
 */


public class GeofireAdapter extends GeofireListAdapter<User> {
    @Bind(R.id.user_name) TextView mUsername;
    @Bind(R.id.track_name) TextView mTrackName;
    @Bind(R.id.user_dp) ImageView mUserDP;
    private Context mContext;

    public GeofireAdapter(Query ref, Activity activity, int layout) {
        super(ref, User.class, layout, activity);
        this.mContext = activity.getApplicationContext();
    }

    /**
     * Bind an instance of the ExampleObject class to our view. This method is called by <code>FirebaseListAdapter</code>
     * when there is a data change, and we are given an instance of a View that corresponds to the layout that we passed
     * to the constructor, as well as a single ExampleObject instance that represents the current data to bind.
     *
     * @param view  A view instance corresponding to the layout we passed to the constructor.
     * @param model An instance representing the current state of a message
     */
    @Override
    protected void populateView(View view, User model) {
        ButterKnife.bind(this, view);

        // populate the list element
        mUsername.setText(model.getName());
        mTrackName.setText(model.getTrackTitle());
        Glide.with(mContext).load(model.getPhotoUrl()).into(mUserDP);
    }

    @Override
    protected List<User> filters(List<User> models, CharSequence filter) {
        List<User> filterList = new ArrayList<>();
        for (int i = 0; i < models.size(); i++) {
            /* implement your own filtering logic
             * and then call  filterList.add(models.get(i));
             */
        }
        return filterList;
    }

    @Override
    protected Map<String, User> filterKeys(List<User> models) {
        return null;
    }
}
