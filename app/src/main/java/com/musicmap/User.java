package com.musicmap;

import java.util.HashMap;

public class User {
    private String fcmUserDeviceId;
    private String name;
    private String email;
    private String photoUrl;
    private String albumArtUrl;
    private String trackTitle;
    private String trackAlbum;
    private String trackUri;
    private HashMap<String, Object> timestampJoined;
    private boolean hasLoggedInWithPassword;
    private boolean online;
    private boolean playingTrack;

    public User() {
    }

    public User(String fcmUserDeviceId, String name, String email, String photoUrl,
                HashMap<String, Object> timestampJoined) {
        this.fcmUserDeviceId = fcmUserDeviceId;
        this.name = name;
        this.email = email;
        this.photoUrl = photoUrl;
        this.timestampJoined = timestampJoined;
        this.hasLoggedInWithPassword = false;
        this.online = false;
        this.playingTrack = false;
    }
    public User(String name, String email, String fcmUserDeviceId, Boolean hasLoggedInWithPassword,
                String photoUrl, Boolean online, boolean playingTrack, HashMap<String, Object> timestampJoined,
                String albumArtUrl, String trackAlbum, String trackTitle, String trackUri) {
        this.name = name;
        this.email = email;
        this.fcmUserDeviceId = fcmUserDeviceId;
        this.hasLoggedInWithPassword = hasLoggedInWithPassword;
        this.timestampJoined = timestampJoined;
        this.photoUrl = photoUrl;
        this.online = online;
        this.playingTrack = playingTrack;
        this.albumArtUrl = albumArtUrl;
        this.trackAlbum = trackAlbum;
        this.trackTitle = trackTitle;
        this.trackUri = trackUri;

    }

    public String getFcmUserDeviceId() {
        return fcmUserDeviceId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public HashMap<String, Object> getTimestampJoined() {
        return timestampJoined;
    }

    public boolean isHasLoggedInWithPassword() {
        return hasLoggedInWithPassword;
    }

    public boolean isPlayingTrack() {
        return playingTrack;
    }

    public boolean isOnline() {
        return online;
    }

    public String getAlbumArtUrl() {
        return albumArtUrl;
    }

    public String getTrackTitle() {
        return trackTitle;
    }

    public String getTrackAlbum() {
        return trackAlbum;
    }

    public String getTrackUri() {
        return trackUri;
    }
}
